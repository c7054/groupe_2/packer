packer {
  required_plugins {
    virtualbox = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}

source "virtualbox-ovf" "ubuntu" {
  source_path      = "Kaisen Linux Client.ova"
  checksum         = "03d2d48f2423d2cbd7bc058af491adcc"
  ssh_username     = "user"
  ssh_password     = "user"
  shutdown_command = "echo 'user' | sudo -S shutdown -P now"
}

build {
  name = "vbox-packer"
  sources = [
    "source.virtualbox-ovf.ubuntu"
  ]
  provisioner "shell" {
    environment_vars = [
      "FOO=hello world",
    ]
    inline = [
      "echo Adding file to ubuntu VM",
      "echo \"FOO is $FOO\" > example.txt",
    ]
  }
  provisioner "shell" {
    inline = ["echo This provisioner runs last"]
  }

}
